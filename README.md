#  Steve Musial WordPress Starter Theme: Timber + Pattern Lab

A custom WordPress starter theme with integrated [Pattern Lab](https://patternlab.io) providing the templates for [WordPress](https://wordpress.org), using [Timber](https://www.upstatement.com/timber/) and [Twig](https://twig.symfony.com). It uses [Composer](https://getcomposer.org) and [NPM](https://nodejs.org) to handle dependencies, and [webpack](https://webpack.js.org) for bundling modules/assets. Use of this theme assumes familiarity with these things.

Note: this starter theme, for purposes of using it as a demo, has been styled a little bit, and some of the templates have been edited, compared to its initial state. It is still relatively bare-bones. Run Pattern Lab to see some placeholder content.

## How to use

### Installation

```bash
composer install
npm install
cd patternlab
composer install
cd ..
npm run pl:build
npm run wp:build
```

Install theme files to ``themes`` directory as normal. Running ``composer install`` from the theme root will install Timber in ``wp-content/mu-plugins``, along with the [Bedrock Autoloader](https://github.com/roots/bedrock/blob/master/web/app/mu-plugins/bedrock-autoloader.php) by [Roots](https://roots.io), which is required to autoload the Timber library from the ``mu-plugins`` directory. Edit composer.json to change this, if desired; if you decide to install Timber in the standard WordPress ``plugins`` directory, the autoloader isn’t required, and in this case it’s also an option to install Timber using the WordPress admin.

Run ``npm install`` from the theme root to install node modules.

To set up Pattern Lab, run ``composer install`` from the ``patternlab`` directory. If the installer asks to update config for the ``styleguideKitPath``, there’s no need to do so, unless you need to use a full system path to the style guide kit (the installer just changes a relative path to a full path). If the installer asks to update the config option ``plugins.twigNamespaces``, choose “n”, as choosing “Y” will disable Twig namespaces, which are enabled in the ``config.yml`` file included in this theme, and which are required for this theme to work.

Once Pattern Lab is set up, navigate back to the theme root and ``npm run pl:build`` to generate the Pattern Lab files. This is important for the next step, as building the theme runs [PurgeCSS](https://www.purgecss.com), which uses Pattern Lab’s HTML files to decide which unused styles to remove from the theme’s compiled CSS.

The last step is ``npm run wp:build`` to build the theme for WordPress. Once that’s done, activate the theme.

### Twig Namespaces

Twig namespaces are the magic which lets Pattern Lab act both as a standalone site and as the templates for WordPress. These namespaces are set in ``patternlab/config/config.yml`` and ``resources/includes/theme_setup.php``. If you want to edit the namespaces, be sure to do so in both files, so they match.

### NPM Scripts for Build Tasks

Package.json includes a number of NPM scripts to separately build Pattern Lab and WordPress, so one can work in Pattern Lab and build for WordPress when ready. Feel free to delete/update and use whatever scripts and task runners you like.

To build Pattern Lab: ``npm run pl:build``

To serve Pattern Lab using Browsersync and watch for changes to patterns, styles, and JS: ``npm run pl:start``

To build for WordPress: ``npm run wp:build``

To serve WordPress using webpack via Browsersync and watch for changes: ``npm run wp:start``

Note that the config in ``webpack.config.js`` used by ``wp:start`` includes a proxy option for the Browsersync plugin which must be manually edited to match your local WordPress dev URL.

To generate a favicon from [RealFaviconGenerator](https://realfavicongenerator.net): ``npm run favicon``

See ``package.json`` for more NPM scripts.

If you run into trouble with ``parallelshell`` throwing the error “``The "options.cwd" property must be of type string``”, downgrading the ``parallelshell`` version from 3.0.2 to 3.0.1 should solve this.

## Required and Optional WordPress Files

The required files ``index.php`` and ``style.css`` are located in the ``resources`` directory, along with ``functions.php``, a few other template files, and some includes:

* ``theme-setup.php`` enqueues CSS and JS, adds a primary nav, and declares Twig namespaces for Pattern Lab directories
* ``class-mythemetimbersite.php`` declares a custom theme class which extends the base Timber class, adds theme support for WordPress functionality, and adds some variables to the Twig context which are available on every page
* ``cards.php`` provides functions for getting posts from WordPress and formatting them as “cards”, and can be used to display posts in all manner of contexts such as grids and lists
* ``images.php`` adds image sizes and provides a responsive image function to easily get ``sizes`` and ``srcset`` attributes for any image size
* ``menus.php`` provides various functions for building menus, including deriving a submenu from another menu
* ``customizer.php`` and ``gutenberg.php`` remove the interfaces for adding custom CSS and custom colors, respectively, in order to prevent overriding the theme styles; these two files can be edited/deleted as desired

## Default Pattern Lab Files

There are some default pattern files so that WordPress and Pattern lab have something to display, along with some default styles. These are meant to be edited/deleted as desired.

### Default Twig Files

There are a few Twig files to get you started:

* ``patternlab/source/_layouts/base.twig`` is the root template. It includes a few other files:
  * ``header.twig`` includes ``primary-navigation.twig``
  * ``footer.twig`` includes the output of ``wp_footer()``
  * ``_html-head.twig`` adds meta, title, etc., as well as the output of ``wp_head()``, to the ``<head>`` element
* ``index.twig`` extends ``base.twig``, and includes post title and content
* ``primary-navigation.twig`` uses Bootstrap nav markup
* the atoms from default Pattern Lab are mostly unchanged
  * favicon.twig uses markup and images from [RealFaviconGenerator](https://realfavicongenerator.net)

The Pattern Lab naming convention of atoms/molecules/organisms has been left intact. Edit as desired. Note that the directory for atoms has a prefix of “01” instead of the default “00”. This is to make it easy to add a “00-global” directory. Since the version of Pattern Lab (v2) included in this theme adds to the top level of its nav both empty directories and directories which include only files prefixed by underscores (which Pattern Lab ignores for the nav), there is no “00-global” directory included by default. This is also why ``_html-head.twig`` is in the organisms directory, when it might be better organized in a “00-global” directory. Pattern Lab 3 for Twig correctly hides nav for empty directories as well as directories with only hidden files, but doesn’t yet auto-generate links for pattern relationships, which is one of Pattern Lab’s key features. This theme will eventually be updated to use Pattern Lab 3 when this functionality is added and Pattern Lab 3 is otherwise ready to use in this theme. If you’d like to use Pattern Lab 3 in this theme at this time, it should work fine, aside from the aforementioned lack of relationship links.

### Default Sass

A few global Sass files are located in ``patternlab/source/css``. These include partials with WordPress generated classes, including for Gutenberg, for overriding the default WordPress styles, as well as files for variables, functions and mixins. Separate Sass files for the components are located in the various pattern directories. Use/edit/delete as desired.

### Default JS

The file ``patternlab/source/js/theme.js`` is separately built for Pattern Lab and WordPress by two corresponding NPM scripts running webpack.

## Inspiration

* [Sage](https://roots.io/sage) by [Roots](https://roots.io)
* [Emulsify](http://emulsify.info) by [Four Kitchens](https://www.fourkitchens.com)
* Timber [starter theme](https://github.com/timber/starter-theme)
