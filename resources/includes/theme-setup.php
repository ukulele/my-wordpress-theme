<?php
/**
 * MyTheme setup
 *
 * Enqueue scripts and styles, and do other setup tasks.
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

/**
 * Stop loading wp-emoji-release.min.js
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Theme assets
 */
function theme_uri() {
	return str_replace( 'resources', '', get_theme_file_uri() );
}

$distdir   = dirname( __DIR__, 2 ) . '/';
$cssdir    = 'dist/css/';
$jsdir     = 'dist/js/';
$styleuri  = theme_uri() . $cssdir;
$scripturi = theme_uri() . $jsdir;

add_action(
	'wp_enqueue_scripts',
	function () use ( $distdir, $cssdir, $styleuri ) {
		// Clear PHP’s file status cache.
		clearstatcache();

		// Enqueue main theme CSS file.
		wp_enqueue_style( 'theme', $styleuri . 'theme.css', [], filemtime( $distdir . $cssdir . 'theme.css' ), null );

		// Add media-query-specific CSS files if they exist.
		$responsive_css = [
			[ 'theme-mq-xs.css', 'xs', '(max-width: 575.98px)' ],
			[ 'theme-mq-sm.css', 'sm', '(min-width: 576px)' ],
			[ 'theme-mq-md.css', 'md', '(min-width: 768px)' ],
			[ 'theme-mq-lg.css', 'lg', '(min-width: 992px)' ],
			[ 'theme-mq-xl.css', 'xl', '(min-width: 1200px)' ],
		];
		foreach ( $responsive_css as $file ) {
			$filepath = $distdir . $cssdir . $file[0];
			if ( file_exists( $filepath ) ) {
				wp_enqueue_style( 'theme-' . $file[1], $styleuri . $file[0], [], filemtime( $filepath ), $file[2] );
			}
		}

		// Enqueue Google Fonts.
		// To set the version of the enqueue, try to get the content-length
		// of the HTTP response header for the first font file found in the
		// body of the response from fonts.googapis.com, else use the WordPress
		// version number.
		$google_font = 'fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700';
		$google_font_response_body = wp_remote_get( 'https://' . $google_font )['body'];
		if ( $google_font_response_body ) {
			$google_font_file = preg_match( '/https:\/\/.*\.ttf/', $google_font_response_body, $matches );
		}
		if ( $google_font_file ) {
			$google_font_file_content_length = wp_get_http_headers( $matches[0] )->getAll()['content-length'];
		}
		$google_font_enqueue_version = $google_font_file_content_length ?? false;
		wp_enqueue_style( 'google_fonts', '//' . $google_font . '&display=swap', null, $google_font_file_content_length );

		// De-register/de-queue oEmbed.
		wp_dequeue_script( 'wp-embed' );
		wp_deregister_script( 'wp-embed' );

		// Don’t load the default jQuery except for admin pages, or when the admin bar is visible.
		if ( ! is_admin() && ! is_admin_bar_showing() ) {
			wp_dequeue_script( 'jquery' );
			wp_deregister_script( 'jquery' );
		}
	},
	10
);

add_action(
	'wp_enqueue_scripts',
	function () use ( $distdir, $jsdir, $scripturi ) {
		// Enqueue main theme JS file.
		wp_enqueue_script( 'theme', $scripturi . 'theme.js', [], filemtime( $distdir . $jsdir . 'theme.js' ), true );
	},
	100
);

/**
 * Custom menus
 */
add_action(
	'after_setup_theme',
	function () {
		register_nav_menus(
			[
				'primary_navigation' => __( 'Primary Navigation', 'theme' ),
			]
		);
	},
	20
);

/**
 * Twig namespaces for Pattern Lab
 */
add_filter(
	'timber/loader/loader',
	function( $loader ) {
		$loader->addPath( dirname( __DIR__, 2 ) . '/patternlab/source/_patterns/01-atoms', 'atoms' );
		$loader->addPath( dirname( __DIR__, 2 ) . '/patternlab/source/_patterns/02-molecules', 'molecules' );
		$loader->addPath( dirname( __DIR__, 2 ) . '/patternlab/source/_patterns/03-organisms', 'organisms' );
		$loader->addPath( dirname( __DIR__, 2 ) . '/patternlab/source/_patterns/04-templates', 'templates' );
		$loader->addPath( dirname( __DIR__, 2 ) . '/patternlab/source/_layouts', 'layouts' );
		return $loader;
	}
);
