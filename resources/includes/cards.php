<?php
/**
 * Cards
 *
 * Returns post data to use in cards.
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

use function MyTheme\{
	get_responsive_image
};

/**
 * Gets an array of cards.
 *
 * @param  array $posts An array of posts.
 * @param  array $args  Optional. An array of arguments. @see get_card() below.
 * @return array        An array of cards.
 */
function get_cards( $posts, $args = null ) {

	if ( gettype( $posts ) === 'array' ) {
		// Initialize cards array.
		$cards = [];

		foreach ( $posts as $post ) {
			$cards[] = get_card( $post, $args );
		}

		return $cards;
	}
}

/**
 * Gets a single card.
 *
 * @param object $post WP_Post object.
 * @param array  $args  {
 *   Optional. An array of arguments.
 *
 *   @type bool   $img            Whether a card has an image. Default 'false'.
 *   @type string $img_sizes      A registered image size. Default 'medium'.
 *   @type bool   $img_link       Whether a card image is a link. Default 'false'.
 *   @type string $card_body      The main text content of the card, Default 'post_excerpt'.
 *   @type string $taxonomy       A taxonomy associated with a card. Default 'null'.
 *   @type string $taxonomy_label A string describing a card’s taxonomy terms. A value of 'none' will exclude it. Default 'null'.
 * }
 * @return array
 */
function get_card( $post, $args ) {

	// Default arguments.
	$img            = $args['img'] ?? false;
	$img_sizes      = $args['img_sizes'] ?? [ 'medium' ];
	$img_link       = $args['img_link'] ?? false;
	$card_body      = $args['body'] ?? 'post_excerpt';
	$taxonomy       = $args['taxonomy'] ?? null;
	$taxonomy_label = $args['taxonomy_label'] ?? null;

	// Set taxonomy label, unless it is set to 'none' or a custom value.
	if ( 'none' !== $taxonomy_label && ! $taxonomy_label ) {
		if ( 'category' === $taxonomy ) {
			$taxonomy_label = 'Categories';
		} elseif ( 'post_tag' === $taxonomy ) {
			$taxonomy_label = 'Tags';
		} else {
			$taxonomy_label = ucwords( $taxonomy );
		}
	}

	// If array item is a post ID, get its post object,
	// else, get the ID from the post object.
	if ( gettype( $post ) === 'integer' ) {
		$id   = $post;
		$post = get_post( $id );
	} else {
		$id = $post->ID;
	}

	// Set card data.
	$title = $post->post_title;
	$body  = $post->$card_body;
	$link  = get_permalink( $id );
	$date  = $post->post_date;
	$image = null;

	if ( $img ) {
		$image_id = get_post_thumbnail_id( $id );
		if ( $image_id ) {
			$image = get_responsive_image( $image_id, $img_sizes );

			if ( $img_link ) {
				$image['link'] = true;
			}
		}
	}

	if ( $taxonomy ) {
		$terms = get_the_terms( $id, $taxonomy );
		if ( $terms ) {
			foreach ( $terms as $t ) {
				$taxonomy_terms[] = [
					'name' => ucwords( $t->name ),
					'link' => get_term_link( $t->term_id ),
				];
			}
		}
	}

	$card = [
		'title'          => $title,
		'body'           => $body,
		'link'           => $link,
		'date'           => $date,
		'image'          => $image,
		'taxonomy'       => $taxonomy_terms,
		'taxonomy_label' => $taxonomy_label,
	];

	return $card;
}
