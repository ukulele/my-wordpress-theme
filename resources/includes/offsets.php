<?php
/**
 * Pagination and offsets
 *
 * Includes general functions as well as settings for specific pages.
 *
 * @link https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

use WP_Query;

/**
 * Adds a custom query variable to store the number of posts offset.
 *
 * @param  array $qvars An array of query variables.
 * @return array The updated array of query variables.
 */
function custom_query_vars( $qvars ) {
	$qvars[] = 'number_of_posts_offset';
	return $qvars;
}
add_filter( 'query_vars', __NAMESPACE__ . '\custom_query_vars' );

/**
 * Adjusts pagination when a query’s number of posts is offset.
 *
 * @param  object $query  A WP_Query object.
 * @param  int    $offset The number of posts offset.
 * @param  int    $ppp    The number of posts per page.
 */
function paginate_offset_posts( $query, $offset, $ppp ) {
	// Detect and handle pagination.
	if ( $query->is_paged ) {

		// Manually determine page query offset (offset + current page (minus one) x posts per page).
		$page_offset = $offset + ( ( $query->query_vars['paged'] - 1 ) * $ppp );

		// Apply adjust page offset.
		$query->set( 'offset', $page_offset );
	} else {
		// This is the first page. Just use the offset...
		$query->set( 'offset', $offset );
	}
	$query->set( 'number_of_posts_offset', $offset );

	// Reduce the number of found posts by the number of offset posts.
	add_filter(
		'found_posts',
		function( $found_posts, $query ) {
			return $found_posts - get_query_var( 'number_of_posts_offset' );
		},
		1,
		2
	);
}

/**
 * Custom settings for posts page.
 *
 * @param object $query A WP_Query object.
 * @return void
 */
function posts_page_query_settings( &$query ) {

	if ( $query->is_main_query() && $query->is_home() ) {

		// Set taxonomy.
		$query->set( 'category_name', 'news' );

		// Set number of posts per page, and corresponding variable to calculate pagination offset.
		$query->set( 'posts_per_page', '10' );

		// Handle pagination.
		paginate_offset_posts( $query, 1, 10 );
	}
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\posts_page_query_settings', 1 );

/**
 * Custom settings for archive pages.
 *
 * @param object $query A WP_Query object.
 * @return void
 */
function archive_page_query_settings( &$query ) {

	if ( $query->is_main_query() && $query->is_archive() ) {

		// Set number of posts per page, and corresponding variable to calculate pagination offset.
		$query->set( 'posts_per_page', '10' );
	}
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\archive_page_query_settings', 1 );
