<?php
/**
 * Post types
 *
 * Customize core post types.
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

/**
 * Add excerpt functionality to pages.
 */
add_post_type_support( 'page', 'excerpt' );
