<?php
/**
 * Menus
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

/**
 * Checks if post is in a menu.
 *
 * @param string|int $menu      A menu name or ID.
 * @param int        $object_id A post object.
 * @return bool Whether the object is in the menu.
 */
function item_is_in_menu( $menu = null, $object_id = null ) {

	// Get menu object.
	$menu_object = wp_get_nav_menu_items( $menu );

	// Stop if there isn't a menu.
	if ( ! $menu_object ) {
		return false;
	}

	// Get the object_id field out of the menu object, as integers.
	$menu_items = array_map( 'intval', wp_list_pluck( $menu_object, 'object_id' ) );

	// Use the current post if object_id is not specified.
	if ( ! $object_id ) {
		global $post;
		$object_id = get_queried_object_id();
	}

	// Test if the specified page is in the menu or not. return true or false.
	return in_array( $object_id, $menu_items, true );
}

/**
 * Returns the ID of a menu item’s menu_item_parent.
 *
 * @param string|int $menu      Menu name, id, or slug.
 * @param int        $object_id Object_id of page.
 * @return int The menu item’s menu_item_parent.
 */
function get_nav_menu_item_parent_id( $menu, $object_id ) {
	$nav_menu_items = wp_get_nav_menu_items( $menu );

	// Stop if there isn’t a menu.
	if ( ! $nav_menu_items ) {
		return false;
	}

	// Get the object_id field out of the menu object as an integer.
	$nav_items_object_ids = array_map( 'intval', wp_list_pluck( $nav_menu_items, 'object_id' ) );

	if ( in_array( $object_id, $nav_items_object_ids, true ) ) {
		foreach ( $nav_menu_items as $item ) {
			$debug = intval( $item->object_id );
			if ( intval( $item->object_id ) === $object_id ) {
				return $item->menu_item_parent;
			}
		}
	}

	return false;
}

/**
 * Returns all child nav_menu_items under a specific parent.
 *
 * @param int   $parent_id the parent nav_menu_item ID.
 * @param array $nav_menu_items An array of nav_menu_items.
 * @param bool  $depth Whether a nav_menu_item has children. Default 'false'.
 * @return array A filtered array of nav_menu_items.
 */
function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = false ) {
	$nav_menu_item_list = array();

	foreach ( (array) $nav_menu_items as $nav_menu_item ) {
		if ( $nav_menu_item->menu_item_parent === $parent_id ) {
			$nav_menu_item_list[] = $nav_menu_item;
			if ( $depth ) {
				$children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items );
				if ( $children ) {
					$nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
				}
			}
		}
	}

	return $nav_menu_item_list;
}

/**
 * Returns a multidimensional array of nav_menu_items from a single-dimension array of nav_menu_items.
 *
 * @param array $flat_nav  A single-dimension array of nav_menu_items.
 * @param int   $parent_id Menu_item_parent of a nav_menu_item.
 * @return array A multidimensional array of nav_menu_items.
 */
function build_menu_tree( array &$flat_nav, $parent_id = 0 ) {
	$branch = [];

	foreach ( $flat_nav as &$nav_item ) {
		if ( $nav_item->menu_item_parent === $parent_id ) {
			$children = build_menu_tree( $flat_nav, $nav_item->ID );
			if ( $children ) {
				$nav_item->children = $children;
			}

			$branch[] = $nav_item;
			unset( $nav_item );
		}
	}

	return $branch;
}

/**
 * Returns a multidimensional array of nav_menu_items from a single-dimension array of nav_menu_items.
 *
 * This function relies on using Custom Links that don’t link to anything for dropdown toggles in nav_menus.
 * Menu items without children don’t have these Custom Links as menu item parents, and so it is possible to
 * identify these childless menu items (for which this function may be useless) by whether their menu_item_parent
 * has a value of zero. For menus which have their dropdown toggles do double-duty as links, it may be necessary
 * to modify this function.
 *
 * Note also that it is assumed that there is a single item in the supplied nav_menu which is considered a
 * “section ancestor” of the returned menu, is also the parent in the WordPress page hierarchy of the rest of
 * the subnav items, and which is at the same level in the supplied nav_menu as the rest of the subnav items.
 * It may be necessary to modify this function for menus which are organized differently.
 *
 * @param array              $post        A post object.
 * @param int|string|WP_Term $nav         A menu ID, slug, name, or object; the nav_menu from which to derive a subnav.
 * @param string             $subnav_name The slug to use for the subnav.
 * @return int A multidimensional array of nav_menu_items.
 */
function get_subnav_from_nav( $post, $nav, $subnav_name = 'dynamic_subnav' ) {

	// Get top-level parent (ancestor) of the post.
	$post_ancestors = get_post_ancestors( $post );
	$section_ancestor_id = end( $post_ancestors ) ?: $post->ID;

	// Return false if the ancestor isn't in the nav.
	if ( ! item_is_in_menu( $nav, $section_ancestor_id ) ) {
		return false;
	}

	// Return false if the ancestor has no menu item parent, since it won't have children.
	if ( get_nav_menu_item_parent_id( $nav, $section_ancestor_id ) === 0 ) {
		return false;
	}

	// Get the nav's menu items.
	$navitems = wp_get_nav_menu_items( $nav );

	// Find the ancestor in the nav.
	foreach ( $navitems as $item ) {
		if ( intval( $item->object_id ) === $section_ancestor_id ) {
			$section_ancestor_menu_parent_id = $item->menu_item_parent;
			break;
		}
	}

	// Generate an array of post objects from the ancestor.
	$section_menu_items = get_nav_menu_item_children( $section_ancestor_menu_parent_id, $navitems );

	// Then, build a hierarchical tree from those items.
	$section_nav_tree = build_menu_tree( $section_menu_items, $section_ancestor_menu_parent_id );

	// Delete the subnav menu if it exists.
	if ( wp_get_nav_menu_object( $subnav_name ) ) {
		wp_delete_nav_menu( $subnav_name );
	}

	// Create a new menu for the subnav.
	wp_update_nav_menu_object( 0, [ 'menu-name' => $subnav_name ] );

	// Get the new menu object.
	$subnav = wp_get_nav_menu_object( $subnav_name );

	// Get ID of new menu object.
	$subnav_menu_id = $subnav->term_id;

	// Get the ID of the menu item which is either the current page, or the top-level ancestor of
	// the current page and also a child of the section ancestor (i.e., the second-to-last item in
	// the post_ancestors array, if that array has more than one item).
	$top_level_ancestor = count( $post_ancestors ) > 1 ? $post_ancestors[ count( $post_ancestors ) - 2 ] : $post->ID;

	// Add items to the new menu object.
	foreach ( $section_nav_tree as $menu_item ) {
		$menu_item_data = [
			'menu-item-title'  => $menu_item->title,
			'menu-item-url'    => $menu_item->url,
			'menu-item-status' => $menu_item->post_status,
		];
		// If the top-level ancestor isn't the section ancestor, display its children (the section
		// ancestor's children are already displayed by default).
		if ( $top_level_ancestor !== $section_ancestor_id && intval( $menu_item->object_id ) === $top_level_ancestor ) {
			$args = [
				'post_parent' => $top_level_ancestor,
				'post_type'   => 'any',
				'post_status' => 'publish',
				'orderby'     => 'menu_order',
				'order'       => 'ASC',
			];
			$menu_item->children = get_children( $args );
		}
		$post_id = wp_update_nav_menu_item( $subnav_menu_id, 0, $menu_item_data );
		if ( $menu_item->children ) {
			foreach ( $menu_item->children as $menu_item ) {
				$this_post_ancestors = get_post_ancestors( $menu_item );
				$title = $menu_item->title ?: $menu_item->post_title;
				$link = 'nav_menu_item' === $menu_item->post_type ? get_permalink( $menu_item->object_id ) : get_permalink( $menu_item->ID );
				$menu_item_data = [
					'menu-item-parent-id' => $post_id,
					'menu-item-title'     => $title,
					'menu-item-url'       => $link,
					'menu-item-status'    => $menu_item->post_status,
				];
				wp_update_nav_menu_item( $subnav_menu_id, 0, $menu_item_data );
			}
		}
	}

	return $subnav_menu_id;
}
