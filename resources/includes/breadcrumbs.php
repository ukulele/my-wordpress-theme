<?php
/**
 * Breadcrumb functions
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

/**
 * Gets a post’s breadcrumbs.
 *
 * @param object $post WP_Post.
 * @return false|void
 */
function get_post_breadcrumbs( $post ) {
	$frontpage_id = (int) get_option( 'page_on_front' );

	// If page isn’t the home page, initialize breadcrumbs array and add the current page to it.
	if ( (int) get_option( 'page_on_front' ) === $post->ID ) {
		return false;
	} else {
		$breadcrumbs[] = [
			'label' => $post->post_title,
			'url'   => get_permalink( $post->ID ),
		];
	}

	// Check whether post has ancestors.
	$breadcrumb_ids = get_post_ancestors( $post );
	if ( $breadcrumb_ids ) {

		// If so, add their crumbs to the array.
		foreach ( $breadcrumb_ids as $crumb ) {
			$crumb_post = get_post( $crumb );
			array_unshift(
				$breadcrumbs,
				[
					'label' => $crumb_post->post_title,
					'url'   => get_permalink( $crumb_post->ID ),
				]
			);
		}
	}

	// Add the ‘Home’ crumb to the beginning of the array.
	array_unshift(
		$breadcrumbs,
		[
			'label' => 'Home',
			'url'   => site_url(),
		]
	);

	return $breadcrumbs;
}

/**
 * Creates breadcrumbs for an archive page.
 *
 * @param  int $term_id ID of taxonomy term.
 * @return array An array of breadcrumbs.
 */
function get_archive_breadcrumbs( $term_id ) {
	$term = get_term( $term_id );

	// Initialize breadcrumbs array and add the ‘Home’ crumb to it.
	$breadcrumbs[] = [
		'label' => 'Home',
		'url'   => site_url(),
	];

	// Display a prefix for the crumb based on taxonomy type.
	if ( 'category' === $term->taxonomy ) {
		$crumb_prefix = 'Category';
	} elseif ( 'post_tag' === $term->taxonomy ) {
		$crumb_prefix = 'Tag';
	} else {
		$crumb_prefix = $term->taxonomy;
	}

	// Add term crumb to end of the array.
	$breadcrumbs[] = [
		'label' => ucwords( $crumb_prefix ) . ': ' . ucwords( $term->name ),
		'url'   => get_term_link( $term_id ),
	];

	return $breadcrumbs;
}
