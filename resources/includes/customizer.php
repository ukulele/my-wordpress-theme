<?php
/**
 * Customizer
 *
 * Customize the Customizer settings.
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

/**
 * Removes the additional CSS section, introduced in 4.7, from the Customizer.
 *
 * @param object $wp_customize WP_Customize_Manager.
 * @return void
 */
function prefix_remove_css_section( $wp_customize ) {
	$wp_customize->remove_section( 'custom_css' );
}
add_action( 'customize_register', __NAMESPACE__ . '\prefix_remove_css_section', 15 );
