<?php
/**
 * MyTheme subclass of Timber\Site
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

use TimberMenu;
use TimberSite;

/**
 * Extends the Timber\Site class.
 */
class MyThemeTimberSite extends TimberSite {

	/**
	 * Constructs the Timber\Site object and applies custom settings.
	 */
	public function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		parent::__construct();
	}

	/**
	 * Adds data to the main Timber context.
	 *
	 * @param array $context The Timber context.
	 * @return array The updated Timber context.
	 */
	public function add_to_context( $context ) {

		// Site info.
		$context['site'] = $this;

		// Navigation.
		$context['nav_primary'] = new TimberMenu( 'main' );

		// Path to theme directory.
		$context['themeuri'] = theme_uri();

		// Path to directory dist/images.
		$context['imageuri'] = theme_uri() . 'dist/images/';

		// Path to directory dist/scripts.
		$context['scripturi'] = theme_uri() . 'dist/scripts/';

		// If the Advanced Custom Fields plugin is installed and active,
		// add to the context all the custom option fields.
		if ( class_exists( 'ACF' ) ) {
			$context['options'] = get_fields( 'option' );

			// Strip tags from ACF WYSIWYG footer blocks.
			$footer_blocks = $context['options']['footer_blocks'];
			if ( $footer_blocks ) {
				$updated_blocks = [];
				foreach ( $footer_blocks as $block ) {
					$block['body']    = strip_tags( $block['body'], '<p><br><a>' );
					$updated_blocks[] = $block;
				}
				$context['options']['footer_blocks'] = $updated_blocks;
			}

			// Get global social media icons from ACF.
			$social_media_object = $context['options']['social_media'];
			if ( $social_media_object ) {

				foreach ( $social_media_object as $icon ) {
					$id      = $icon['account']->ID;
					$service = get_field( 'service', $id );
					$url     = get_field( 'url', $id );
					$label   = get_the_title( $id );

					$icon_data = array(
						'service' => $service,
						'url'     => $url,
						'label'   => $label,
					);

					$context['social_media_icons'][] = $icon_data;
				}
			}
		}

		return $context;
	}

}

new MyThemeTimberSite();
