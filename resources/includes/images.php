<?php
/**
 * Images
 *
 * Image settings and functions.
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

// If the post has a featured image, add a corresponding class to the body element.
add_filter(
	'body_class',
	function( $classes ) {
		if ( has_post_thumbnail() ) {
			return array_merge( $classes, array( 'featured-image' ) );
		} else {
			return $classes;
		}
	}
);

// Custom image sizes.
add_image_size( '320px-wide', 320, 0, true );
add_image_size( '384px-wide', 384, 0, true );
add_image_size( '480px-wide', 480, 0, true );
add_image_size( '640px-wide', 640, 0, false );
add_image_size( '700px-wide', 700, 0, false );
add_image_size( '768px-wide', 768, 0, false );
add_image_size( '1024px-wide', 1024, 0, false );
add_image_size( 'crop-384x192', 384, 192, true );

/**
 * Gets a responsive image.
 *
 * @param int   $id          The ID of the image.
 * @param array $image_sizes A list of image sizes to use.
 * @return false:array The responsive image attributes.
 */
function get_responsive_image( $id, $image_sizes = array( 'large' => '100vw' ) ) {
	if ( ! wp_attachment_is_image( $id ) ) {
		return false;
	} else {

		// Get image alt value.
		$alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

		// Get rendered sizes from image metadata.
		$meta = wp_get_attachment_metadata( $id );
		$rendered_image_sizes = $meta['sizes'];

		// Get all available image sizes, including ‘full’, which we have to add manually because
		// the function get_intermediate_image_sizes() doesn’t include it.
		$available_image_sizes = get_intermediate_image_sizes();
		array_push( $available_image_sizes, 'full' );

		// Initialize arrays to use while looping over desired image sizes.
		$image_widths       = array();
		$image_sizes_array  = array();
		$image_srcset_array = array();

		foreach ( $image_sizes as $size => $vw ) {
			// If a viewport width isn’t provided (and thus the key is an integer),
			// set the key equal to the value (which is the image size label) and assign a value of 100vw.
			if ( is_int( $size ) ) {
				$size = $vw;
				$vw   = '100vw';
			}

			// If the image size has been rendered, add it to srcset and sizes.
			if ( in_array( $size, $available_image_sizes, true ) && ! is_null( $rendered_image_sizes ) && array_key_exists( $size, $rendered_image_sizes ) ) {
				$image_url   = wp_get_attachment_image_src( $id, $size )[0];
				$image_width = wp_get_attachment_image_src( $id, $size )[1];

				array_push( $image_widths, $image_width );
				array_push( $image_srcset_array, $image_url . ' ' . $image_width . 'w' );
				array_push( $image_sizes_array, '(max-width: ' . $image_width . 'px) ' . $vw );
			}
		}

		// If at least one existing image size was supplied for the $image_sizes argument, use the values from
		// the loop, otherwise get the values of the 'large' image size using core WordPress functions.
		if ( ! empty( $image_widths ) ) {
			// Get width of widest image.
			$image_sizes_max_width = strval( max( $image_widths ) ) . 'px';

			// Set srcset and sizes attributes.
			$image_srcset = implode( ',', $image_srcset_array );
			$image_sizes  = implode( ',', $image_sizes_array ) . ', ' . $image_sizes_max_width;
		} else {
			// Get the srcset and sizes attributes using core WordPress functions.
			$image_srcset = wp_get_attachment_image_srcset( $id, 'large' );
			$image_sizes  = wp_get_attachment_image_sizes( $id, 'large' );
		}

		// Set the default source to large image size; index 0 of wp_get_attachment_image_src returns a URL.
		$responsive_image['src'] = wp_get_attachment_image_src( $id, 'large' )[0];

		// Set other image attributes.
		$responsive_image['alt']    = $alt;
		$responsive_image['srcset'] = $image_srcset;
		$responsive_image['sizes']  = $image_sizes;

		return $responsive_image;
	}
}
