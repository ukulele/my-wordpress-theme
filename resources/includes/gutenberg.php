<?php
/**
 * Gutenberg
 *
 * Customize the Gutenberg settings.
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

namespace MyTheme;

/**
 * Removes custom colors from the Gutenberg editor.
 */
function setup_theme_supported_features() {
	add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\setup_theme_supported_features' );
