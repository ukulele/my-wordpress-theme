<?php
/**
 * MyTheme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

/**
 * If Timber is not activated
 */
if ( ! class_exists( 'Timber' ) ) {
	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function( $template ) {
			return get_stylesheet_directory() . '/no-timber.html';
		}
	);

	return;
}

/**
 * Includes
 */

// Theme setup.
require_once __DIR__ . '/includes/theme-setup.php';

// Timber setup.
require_once __DIR__ . '/includes/class-mythemetimbersite.php';

// Menus.
require_once __DIR__ . '/includes/menus.php';

// Breadcrumbs.
require_once __DIR__ . '/includes/breadcrumbs.php';

// Image processing.
require_once __DIR__ . '/includes/images.php';

// Custom settings for core post types.
require_once __DIR__ . '/includes/post-types.php';

// Customizer.
require_once __DIR__ . '/includes/customizer.php';

// Custom Gutenberg settings.
require_once __DIR__ . '/includes/gutenberg.php';

// Custom offsets for pagination.
require_once __DIR__ . '/includes/offsets.php';

// Cards.
require_once __DIR__ . '/includes/cards.php';
