const path = require('path');
const TerserJSPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, '../'),
  entry: {
    theme: [
      './patternlab/source/js/theme.js'
    ]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '../patternlab/public/js')
  },
  optimization: {
    minimizer: [
      new TerserJSPlugin({
        terserOptions: {
          output: {
            comments: false,
          },
        },
      }),
    ],
  },
  mode: 'development',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.js$/,
        include: /node_modules\/bootstrap/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader?name=images/[name].[ext]'
          },
        ],
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin()
  ],
};