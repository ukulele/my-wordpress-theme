<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

use function MyTheme\{
	get_responsive_image,
	get_subnav_from_nav,
	get_post_breadcrumbs
};

$context = Timber::get_context();
$context['post'] = new TimberPost();

// Get the featured image.
$post_featured_image_id = get_post_thumbnail_id();
$featured_image_sizes = array(
	'320px-wide',
	'384px-wide',
	'480px-wide',
	'640px-wide',
	'700px-wide',
	'768px-wide',
	'1024px-wide',
);
$context['post_featured_img'] = get_responsive_image( $post_featured_image_id, $featured_image_sizes );

// Get subnav is there is one.
$subnav_menu_id = function_exists( 'get_field' ) ? get_field( 'subnav_menu_id' ) : null;
$subnav_menu_id = is_null( $subnav_menu_id ) ? get_subnav_from_nav( $post, get_nav_menu_locations()['primary_navigation'] ) : null;
if ( $subnav_menu_id ) {
	$context['nav_secondary']     = new TimberMenu( $subnav_menu_id );
	$context['has_sidebar_class'] = true;
}

// Get breadcrumbs if they exist.
$breadcrumbs = get_post_breadcrumbs( $post );
if ( $breadcrumbs ) {
	$context['breadcrumbs']           = $breadcrumbs;
	$context['has_breadcrumbs_class'] = true;
}

// Get the featured cards if they exist.
$featured_cards = function_exists( 'get_field' ) ? get_field( 'featured_cards' ) : null;
if ( $featured_cards ) {
	$args = [
		'img'       => true,
		'img_sizes' => [ 'crop-384x192' ],
		'img_link'  => true,
	];
	$context['post']->card_grid = get_cards( $featured_cards, $args );
}

Timber::render( '@templates/index.twig', $context );
