<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['post']->content = '<h1>Page Not Found</h1><p>We’re sorry. We could not find the page you were looking for.</p>';
Timber::render( '@templates/index.twig', $context );
