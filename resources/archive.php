<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

use function MyTheme\{
	get_archive_breadcrumbs
};

$context = Timber::get_context();

// Get the taxonomy term from WP_Query.
$taxonomy_term                = $wp_query->queried_object;
$taxonomy_term_name_formatted = ucwords( $taxonomy_term->name );
$taxonomy_term_type           = $taxonomy_term->taxonomy;

// Set the page title prefix.
if ( 'category' === $taxonomy_term_type ) {
	$page_title_prefix = 'Items in Category';
} elseif ( 'post_tag' === $taxonomy_term_type ) {
	$page_title_prefix = 'Items Tagged';
} else {
	$page_title_prefix = 'Items Matching';
}

// Get the archive cards.
$archive_posts_args = [
	'taxonomy' => $taxonomy_term_type,
];
$context['archive_posts'] = get_cards( $posts, $archive_posts_args );

// Get breadcrumbs if they exist.
$breadcrumbs = get_archive_breadcrumbs( $taxonomy_term->term_id );
if ( $breadcrumbs ) {
	$context['breadcrumbs']           = $breadcrumbs;
	$context['has_breadcrumbs_class'] = true;
}

// Set the page title.
$context['page_title'] = $page_title_prefix . ' “' . $taxonomy_term_name_formatted . '”';
$context['has_page_title_class'] = true;

Timber::render( '@templates/archive.twig', $context );
