const path = require('path');
const purgecssCommon = require('./purgecss-common.config')

module.exports = {
  content: [path.resolve(__dirname, '../patternlab/public/patterns/**/*.html')],
  css: [path.resolve(__dirname, '../patternlab/public/css/theme.css')],
  whitelistPatternsChildren: purgecssCommon.whitelistPatternsChildren
}