const path = require('path');
const glob = require('glob')
const TerserJSPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MediaQueryPlugin = require('media-query-plugin');
const NodeSassGlobImporter = require('node-sass-glob-importer');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin')
const purgecssCommon = require('./purgecss-common.config')

module.exports = {
  context: path.resolve(__dirname, '../'),
  entry: {
    theme: [
      './patternlab/source/js/theme.js',
      './patternlab/source/css/theme.scss'
    ]
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, '../dist')
  },
  optimization: {
    minimizer: [
      new TerserJSPlugin({
        terserOptions: {
          output: {
            comments: false,
          },
        },
      }),
      new OptimizeCssAssetsPlugin({})
    ],
  },
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.js$/,
        include: /node_modules\/bootstrap/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          MediaQueryPlugin.loader,
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')({})]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              importer: NodeSassGlobImporter()
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader?name=images/[name].[ext]'
          },
        ],
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "css/[id].css"
    }),
    new PurgecssPlugin({
      paths: glob.sync(path.resolve(__dirname, '../patternlab/public/patterns/**/*.html'),  { nodir: true }),
      whitelistPatternsChildren: purgecssCommon.whitelistPatternsChildren
    }),
    new BrowserSyncPlugin( {
        files: [{
          match: ['**/*.css', '**/*.js', '**/*.php', '**/*.twig'],
        }],
        ignore: ['**/vendor/**/*', '**/node_modules/**/*'],
        host: 'localhost',
        port: 5000,
        proxy: 'http://devsite.localdev'
      }
    ),
    new MediaQueryPlugin({
      include: [
        'theme'
      ],
      queries: {
        '(max-width: 575.98px)': 'mq-xs',
        '(min-width: 576px)': 'mq-sm',
        '(min-width: 768px)': 'mq-md',
        '(min-width: 992px)': 'mq-lg',
        '(min-width: 1200px)': 'mq-xl',
      }
    })
  ],
};