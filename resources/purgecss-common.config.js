/**
 * Store common Purgecss config to be used by both
 * the main Purgecss config file (which is used for
 * Pattern Lab's CSS) and by webpack for the
 * WordPress build.
 */

const purgecssCommon = {
  whitelistPatternsChildren: [
    // Bootstrap selectors
    /show/,

    // WordPress selectors
    /blog/,
    /aligncenter/,
    /alignnone/,
    /alignleft/,
    /alignright/,
    /post-content/,
    /screen-reader-text/,
    /^wp-/,
    /menu-item/,
    /menu-item-*/,
    /current-menu-*/,
    /current_page_*/,

    // SVG
    /social-media-icon*/,
    /logo*/,

    // Pattern Lab
    /iframe-pl*/,
    /sg*/,
    /patternLink/,
    /demo-animate/,

    // Custom patterns
    /has-page-title/,
    /has-breadcrumbs/,
    /has-sidebar-left/,
    /has-sidebar-right/,
    /card-horizontal/,
    /card-vertical/,
    /footer-block*/
  ]
}

module.exports = purgecssCommon
