<?php
/**
 * The template for displaying the blog posts index
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#home-page-display
 *
 * @package WordPress
 * @subpackage MyTheme
 * @since 1.0.0
 */

use function MyTheme\{
	get_responsive_image
};

$context = Timber::get_context();
$context['sidebar_right'] = true;

// Display page title, which WordPress doesn’t do by default for the posts page.
$context['page_title'] = get_the_title( get_option( 'page_for_posts' ) );

// Display a post hero only on the first page.
if ( $paged < 2 ) {
	$context['news_home'] = true;
}

// Get the latest blog.
$latest_post_query_args = [
	'post_type'     => 'post',
	'posts_per_pag' => 1,
	'tax_query'     => [
		[
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => 'news',
		],
	],
];
$latest_post_query = new WP_Query( $latest_post_query_args );
$latest_post = $latest_post_query->post;

// Generate the card for the latest blog.
$latest_news_posts_args = [
	'img'       => true,
	'img_sizes' => [ 'large' ],
	'img_link'  => true,
];
$context['latest_news_post'] = get_card( $latest_post, $latest_news_posts_args );

// Generate the cards for the other posts.
$news_posts_args = [
	'img'       => true,
	'img_sizes' => [ 'thumbnail' ],
	'img_link'  => true,
];
$context['news_posts'] = get_cards( $posts, $news_posts_args );

Timber::render( '@templates/home.twig', $context );
