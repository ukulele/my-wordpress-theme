import './components/skip-link';

/**
 * Boostrap.
 *
 * Toggle any desired modules.
 *
 * jQuery and Popper will be imported as needed.
 */
// import 'bootstrap/js/src/alert'
// import 'bootstrap/js/src/button'
// import 'bootstrap/js/src/carousel'
import 'bootstrap/js/src/collapse';

// Import edited version of Bootstrap core dropdown
// import 'bootstrap/js/src/dropdown'
import './components/dropdown';

// import 'bootstrap/js/src/modal'
// import 'bootstrap/js/src/popover'
// import 'bootstrap/js/src/scrollspy'
// import 'bootstrap/js/src/tab'
// import 'bootstrap/js/src/toast'
// import 'bootstrap/js/src/tooltip'

/**
 * Reframe.js.
 *
 * Makes unresponsive elements responsive.
 *
 * {@link https://dollarshaveclub.github.io/reframe.js}
 */
import './components/reframe';

/**
 * svgxuse.
 *
 * Adds to IE11 the ability to load external references in SVG <use> elements.
 *
 * {@link https://github.com/Keyamoon/svgxuse}
 */
import 'svgxuse/svgxuse';

/**
 * Images for Wordpress.
 */
import '../images/social-media-icons.svg';
