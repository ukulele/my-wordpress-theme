/**
 * Custom settings for Reframe.js.
 */
const reframe = require( 'reframe.js/dist/reframe' );
window.addEventListener( 'DOMContentLoaded', function() {
	reframe( 'iframe' );
}, true );
