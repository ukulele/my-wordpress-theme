/**
 * Skips to another page element, typically the main content.
 *
 * Derived from the following:
 * {@link https://webaim.org/techniques/skipnav/}
 * {@link https://axesslab.com/skip-links/}
 * {@link https://codepen.io/200ok/pen/jvNBMP}
 */

if ( document.getElementById( 'skip-link' ) !== null ) {
	document.getElementById( 'skip-link' ).addEventListener( 'click', function( event ) {
		// Prevent default link behavior.
		event.preventDefault();

		// Get target element.
		const target = document.getElementById( this.getAttribute( 'href' ).replace( '#', '' ) );

		// Set tabindex of target to -1 to address a bug in some browsers.
		target.setAttribute( 'tabindex', '-1' );

		// Set focus to target element.
		target.focus();

		// Add event listeners to target to remove tabindex as adding it
		// introduces other problems for tabbed navigation.
		target.addEventListener( 'blur', function( e ) {
			e.preventDefault();
			this.removeAttribute( 'tabindex' );
		} );
		target.addEventListener( 'focusout', function( e ) {
			e.preventDefault();
			this.removeAttribute( 'tabindex' );
		} );
	} );
}
