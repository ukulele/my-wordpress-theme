---
title: Landscape 3x2
---

Landscape-orientation image with a ratio of 3:2. Used primarily for body content.