---
title: Landscape 2x1
---

Landscape-orientation image with a ratio of 2:1. Used primarily for page and card headers.