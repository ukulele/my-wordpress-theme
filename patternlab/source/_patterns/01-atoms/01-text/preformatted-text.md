---
title: Preformatted Text
---

Default style for “text which is to be presented exactly as written in the HTML file” (see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/pre).