---
title: Archive Card
---

Used to display content which has been assigned categories/tags. Typically viewed on an “archive page”.