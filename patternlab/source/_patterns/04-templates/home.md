---
title: Posts Page (Blog Home) Template
---

Template for the blog home page. Displays a list of recent posts. Used for the News page.